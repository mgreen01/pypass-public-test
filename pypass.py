import sys
import os.path
from sqlalchemy import *
import pyperclip
# The First User gretting message.
# print("Welcome to SimplePass: The simple interactive terminal based password manager. /n")

# The general foundation class for every single menu


class foundation_menu:
    def __init__(self, title, options):
        self.title = title
        self.options = options

    def display_menu(self):
        print(self.title)
        for i, option in enumerate(self.options):
            print(f"{i + 1}. {option}")
        return int(input("Please enter one of the following options."))


class entry_menu(foundation_menu):
    def __init__(self):
        super().__init__("Entry Menu: ", [
            "Create new database.", "Find existing database", "Exit"])


class main_menu(foundation_menu):
    def __init__(self):
        super().__init__("Main Menu: ", [
            "Create New User and Password", "Select Login Credentials", "Return", "Exit"])


class details_menu(foundation_menu):
    def __init__(self):
        super().__init__("Details Menu: ", [
            "Copy Username in Clipboard", "Copy Password into Clipboard", "Edit Database Entry", "Return", "Exit"])


class edit_menu(foundation_menu):
    def __init__(self):
        super().__init__("Entry Editing Menu: ", [
            "Edit Username", "Edit Password", "Return to Previous Menu", "Exit"])


class confirm_menu(foundation_menu):
    def __init__(self):
        super().__init__("Are You Sure?: ", ["Yes", "No"])


EntryMenu = entry_menu()
MainMenu = main_menu()
DetailsMenu = details_menu()
EditMenu = edit_menu()
ConfirmMenu = confirm_menu()

while True:
    # Entry Menu
    datadir = "databases/"
    menu = EntryMenu.display_menu()
    if menu == 1:
        userinput = (
            input("Please enter the name of the database that you would like to create: \n"))
        userdata = str(userinput + ".db")
        userconn = str("sqlite:///" + userdata)
        print(userconn)
        # Database creation code begins (The following code down below creates and connects to a SQLite database.)
        engine = create_engine(userconn)
        conn = engine.connect()
        metadata = MetaData()
        Passwords = Table('Passwords', metadata,
                          Column('Id', Integer(), primary_key=True),
                          Column('Username', String(255), nullable=False),
                          Column('Password', String(255), nullable=False)
                          )
        metadata.create_all(engine)
   # Main Menu
        while True:
            menu = MainMenu.display_menu()
            # Fetchs the database records
            sql_menu = text("SELECT * from Passwords")
            result = conn.execute(sql_menu).fetchall()
            for record in result:
                print("\n", record)
            ###
            if menu == 1:
                # SQL Statement that adds the user's username and password
                username_input = input(
                    "Please enter the username you would like to add: \n")
                password_input = input(
                    "Please enter the password you would like to add: \n")
                query = insert(Passwords).values(
                    Username=username_input, Password=password_input)
                result = conn.execute(query)
                conn.commit()
                print("Return to menu. \n")
            elif menu == 2:
                # Allows the user to select an entry within the database
                select_input = input("Please select the database entry: \n")
                # lines 102 - 107 validates if the user inputted value existed within the database. 
                select_input_int = (int(select_input))
                stmt = conn.execute(
                    select(func.count()).select_from(Passwords))
                valid_data = str(stmt.fetchone())
                valid_input = valid_data.strip("(,)")
                valid_input_int = int(valid_input)
                if select_input_int <= valid_input_int:
                    print("Entry ", select_input, " has been selected. \n")
                    menu = DetailsMenu.display_menu()
                    if menu == 1:
                        print("Copies Username entry into a clipboard and clears it after 10 seconds. \n")
                        stmt = select(Passwords.c.Username).where(Passwords.c.Id == select_input)
                        row = conn.execute(stmt).fetchone()
                        row_str = str(row)
                        print(row_str)
                        row_strip=row_str.strip("(',')")
                        print(row_strip)
                        pyperclip.copy(row_strip)
                    elif menu == 2:
                        print("Copies the password entry ino the system and then deletes it from the clipboard after 10 seconds. \n")
                        stmt = select(Passwords.c.Password).where(Passwords.c.Id == select_input)
                        row = conn.execute(stmt).fetchone()
                        row_str = str(row)
                        print(row_str)
                        row_strip=row_str.strip("(',')")
                        pyperclip.copy(row_strip)
                    elif menu == 3:
                        # Edit Menu
                        menu = EditMenu.display_menu()
                        if menu == 1:
                            # Edit Username
                            new_username = input(
                                "Please enter your new username: \n")
                            u = update(Passwords)
                            u = u.values({"Username": new_username})
                            u = u.where(Passwords.c.Id == select_input)
                            conn.execute(u)
                            sql_menu = text("SELECT * from Passwords")
                            result = conn.execute(sql_menu).fetchall()
                            for record in result:
                                print("\n", record)
                            conn.commit()
                            ###
                        elif menu == 2:
                            # Edit Password
                            new_password = input(
                                "Please enter your new password: \n")
                            u = update(Passwords)
                            u = u.values({"Password": new_password})
                            u = u.where(Passwords.c.Id == select_input)
                            conn.execute(u)
                            sql_menu = text("SELECT * from Passwords")
                            result = conn.execute(sql_menu).fetchall()
                            for record in result:
                                print("\n", record)
                            conn.commit()
                            ###
                        elif menu == 3:
                            break
                        elif menu == 4:
                            quit()
                    elif menu == 4:
                        break
                    elif menu == 5:
                        quit()
                else:
                    break
            elif menu == 3:
                break
            elif menu == 4:
                quit()
    if menu == 2:
        datadir = "databases/"
        userinput = input(
            "Please enter the name of the database that you would like to enter?: "
        )
        userdata = str(userinput + ".db")
        while os.path.exists(userdata):
            print("The database has been found.")
            userconn = str("sqlite:///" + userdata)
            engine = create_engine(userconn)
            conn = engine.connect()
            metadata = MetaData()
            Passwords = Table('Passwords', metadata,
                              Column('Id', Integer(), primary_key=True),
                              Column('Username', String(255), nullable=False),
                              Column('Password', String(255), nullable=False)
                              )
            metadata.create_all(engine)
            # Main Menu
            menu = MainMenu.display_menu()
            sql_menu = text("SELECT * from Passwords")
            result = conn.execute(sql_menu).fetchall()
            for record in result:
                print("\n", record)
            if menu == 1:
                username_input = input("Please enter the username you would like to add: \n")
                password_input = input("Please enter the password you would like to add: \n")
                query = insert(Passwords).values(Username=username_input, Password=password_input)
                result = conn.execute(query)
                conn.commit()
            elif menu == 2:
                select_input = input(
                    "Please select the database entry: \n")
                select_input_int = (int(select_input))
                stmt = conn.execute(
                    select(func.count()).select_from(Passwords))
                valid_data = str(stmt.fetchone())
                valid_input = valid_data.strip("(,)")
                valid_input_int = int(valid_input)
                if select_input_int <= valid_input_int:
                        print("Entry ", select_input, " has been selected. \n")
                        menu = DetailsMenu.display_menu()
                        if menu == 1:
                            print("Copies Username entry into a clipboard and clears it after 10 seconds. \n")
                            stmt = select(Passwords.c.Username).where(Passwords.c.Id == select_input)
                            row = conn.execute(stmt).fetchone()
                            row_str = str(row)
                            row_strip=row_str.strip("(',')")
                            pyperclip.copy(row_strip)
                        elif menu == 2:
                            print("Copies the password entry ino the system and then deletes it from the clipboard after 10 seconds. \n")
                            stmt = select(Passwords.c.Password).where(Passwords.c.Id == select_input)
                            row = conn.execute(stmt).fetchone()
                            row_str = str(row)
                            row_strip=row_str.strip("(',')")
                            print(row_strip)
                            pyperclip.copy(row_strip)

                        elif menu == 3:
                                # Edit Menu
                            menu = EditMenu.display_menu()
                            if menu == 1:
                                    # Edit Username Code
                                new_username = input("Please enter your new username: \n")
                                u = update(Passwords)
                                u = u.values({"Username": new_username})
                                u = u.where(Passwords.c.Id == select_input)
                                conn.execute(u)
                                sql_menu = text("SELECT * FROM Passwords")
                                result = conn.execute(sql_menu).fetchall()
                                # Edit Username Code
                                conn.commit()
                            elif menu == 2:
                                # Edit Password Code
                                new_password = input("Please enter your new password: \n")
                                u = update(Passwords)
                                u = u.values({"Password": new_password})
                                u = u.where(Passwords.c.Id == select_input)
                                conn.execute(u)
                                conn.commit()
                                # Edit Password
                            elif menu == 3:
                                break
                            elif menu == 4:
                                quit()
                            elif menu == 4:
                                break
                            elif menu == 5:
                                quit()
                else:
                    break
            elif menu == 3:
                break
            elif menu == 4:
                quit()
    if menu == 3:
        break
